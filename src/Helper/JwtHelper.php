<?php

namespace App\Softexpert\Helper;
use Firebase\JWT\JWT;

class JwtHelper 
{
    public static function generateJWT(array $body): string
    {
        $payload = array(
            'user' => $body[0]['username'],
            'password' => $body[0]['password']
        );

        return JWT::encode($payload, $_ENV['SECRET_KEY'], 'HS256');
    }
}
