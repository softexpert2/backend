<?php

namespace App\Softexpert\Interfaces;

interface iGenericRest
{
    public function getAll();
    public function getOne(int $id);
    public function post($body);
    public function put(int $id, $body);
    public function delete(int $id);
}