<?php
namespace App\Softexpert\Database;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Connection;

/**
 * @mixin Connection
 */
class DB
{
    public Connection $connection;

    public function __construct()
    {
        $connectionParams = [
            'dbname' => $_ENV['DB_DATABASE'],
            'user' => $_ENV['DB_USER'],
            'password' => $_ENV['DB_PASS'],
            'host' => $_ENV['DB_HOST'],
            'driver' => $_ENV['DB_DRIVER'] ?? 'pdo_pgsql',
        ];
        $this->connection = DriverManager::getConnection($connectionParams);
    }
}