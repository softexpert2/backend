<?php

use App\Softexpert\Controllers\CategoryController;
use App\Softexpert\Controllers\ProductController;
use App\Softexpert\Controllers\SellingController;
use App\Softexpert\Controllers\UserController;
use Dotenv\Dotenv;

$dotenv = Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();

header('Content-Type: application/json');

$router = new \Bramus\Router\Router();

// selling
$router->get('/selling/all', function () {
    $controller = new SellingController();
    echo $controller->getAll();
});

$router->get('/selling/all/user/{id}', function ($id) {
    $controller = new SellingController();
    echo $controller->getAllUser($id);
});

$router->get('/selling/find/{id}', function ($id) {
    $controller = new SellingController();
    echo $controller->getOne($id);
});

$router->post('/selling/add', function () {
    $controller = new SellingController();
    $inputJSON = file_get_contents('php://input');
    $body = json_decode($inputJSON);
    $controller->post($body);
});

$router->delete('/selling/delete/{id}', function ($id) {
    $controller = new SellingController();
    $controller->delete($id);
});

// category
$router->get('/category/all', function () {
    $controller = new CategoryController();
    echo $controller->getAll();
});

$router->get('/category/find/{id}', function ($id) {
    $controller = new CategoryController();
    echo $controller->getOne($id);
});

$router->post('/category/add', function () {
    $controller = new CategoryController();
    $inputJSON = file_get_contents('php://input');
    $body = json_decode($inputJSON);
    $controller->post($body);
});

$router->delete('/category/delete/{id}', function ($id) {
    $controller = new CategoryController();
    $controller->delete($id);
});

// products
$router->get('/product/all', function () {
    $controller = new ProductController();
    echo $controller->getAll();
});

$router->get('/product/find/{id}', function ($id) {
    $controller = new ProductController();
    echo $controller->getOne($id);
});

$router->post('/product/add', function () {
    $controller = new ProductController();
    $inputJSON = file_get_contents('php://input');
    $body = json_decode($inputJSON);
    $controller->post($body);
});

$router->put('/product/update/{id}', function ($id) {
    $controller = new ProductController();
    $inputJSON = file_get_contents('php://input');
    $body = json_decode($inputJSON);
    $controller->put($id, $body);
});

$router->delete('/product/delete/{id}', function ($id) {
    $controller = new ProductController();
    $controller->delete($id);
});

// users
$router->get('/users/all', function () {
    $controller = new UserController();
    echo $controller->getAll();
});

$router->get('/users/find/{id}', function ($id) {
    $controller = new UserController();
    echo $controller->getOne($id);
});

$router->post('/users/add', function () {
    $controller = new UserController();
    $inputJSON = file_get_contents('php://input');
    $body = json_decode($inputJSON);
    $controller->post($body);
});

$router->put('/users/update/{id}', function ($id) {
    $controller = new UserController();
    $inputJSON = file_get_contents('php://input');
    $body = json_decode($inputJSON);
    $controller->put($id, $body);
});

$router->delete('/users/delete/{id}', function ($id) {
    $controller = new UserController();
    $controller->delete($id);
});

$router->post('/users/login', function () {
    $controller = new UserController();
    $inputJSON = file_get_contents('php://input');
    $body = json_decode($inputJSON);
    echo $controller->login($body);
});

$router->post('/users/islogin', function () {
    $controller = new UserController();
    $inputJSON = file_get_contents('php://input');
    $body = json_decode($inputJSON);
    echo $controller->isLogin($body);
});

$router->get('/about', function () {
    echo 'About Page Contents';
});

$router->run();