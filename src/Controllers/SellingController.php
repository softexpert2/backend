<?php

namespace App\Softexpert\Controllers;

use App\Softexpert\Database\DB;
use App\Softexpert\Interfaces\iGenericRest;
use ErrorException;

class SellingController implements iGenericRest
{
    public function getAll()
    {
        $conn = new DB();
        $stmt = $conn->connection->prepare('SELECT s.*, u.name AS user_name, p.name AS product_name FROM selling s JOIN users u ON s.user_id = u.id JOIN products p ON s.product_id = p.id');
        return json_encode($stmt->executeQuery()->fetchAllAssociative());
    }

    public function getAllUser($id)
    {
        $conn = new DB();
        $stmt = $conn->connection->prepare('SELECT s.*, u.name AS user_name, p.name AS product_name FROM selling s JOIN users u ON s.user_id = u.id JOIN products p ON s.product_id = p.id WHERE s.user_id = :id');
        $stmt->bindValue(':id', $id);
        return json_encode($stmt->executeQuery()->fetchAllAssociative());
    }

    public function getOne($id)
    {
        $conn = new DB();
        $stmt = $conn->connection->prepare('SELECT s.*, u.name AS user_name, p.name AS product_name FROM selling s JOIN users u ON s.user_id = u.id JOIN products p ON s.product_id = p.id WHERE s.id = :id');
        $stmt->bindValue(':id', $id);
        return json_encode($stmt->executeQuery()->fetchAllAssociative());
    }

    public function post($body)
    {
        $conn = new DB();
        $stmt = $conn->connection->prepare('INSERT INTO selling (user_id, product_id, amount, value_total, value_with_tax )
        VALUES (:user_id, :product_id, :amount, :value_total, :value_with_tax)');

        $stmt->bindValue(':user_id', $body->user_id);
        $stmt->bindValue(':product_id', $body->product_id);
        $stmt->bindValue(':amount', $body->amount);
        $stmt->bindValue(':value_total', $body->value_total);
        $stmt->bindValue(':value_with_tax', $body->value_with_tax);

        return json_encode($stmt->executeQuery()->fetchAllAssociative());
    }

    public function put($id, $body)
    {
        throw new ErrorException("Not Implemented");
    }

    public function delete($id)
    {
        $conn = new DB();
        $stmt = $conn->connection->prepare('DELETE FROM selling WHERE id = :id');
        $stmt->bindValue(':id', $id);

        return json_encode($stmt->executeQuery()->fetchAllAssociative());
    }

}