<?php
namespace App\Softexpert\Controllers;

use App\Softexpert\Database\DB;
use App\Softexpert\Helper\JwtHelper;
use App\Softexpert\Interfaces\iGenericRest;
use DateInterval;
use DateTime;

class UserController implements iGenericRest
{

    public function getAll()
    {
        $conn = new DB();
        $stmt = $conn->connection->executeQuery('SELECT * FROM users');
        return json_encode($stmt->fetchAllAssociative());
    }
    public function getOne($id)
    {
        $conn = new DB();
        $stmt = $conn->connection->prepare('SELECT * FROM users WHERE id = :id');
        $stmt->bindValue(':id', $id);
        return json_encode($stmt->executeQuery()->fetchAllAssociative());
    }

    public function post($body)
    {
        $conn = new DB();
        $stmt = $conn->connection->prepare('INSERT INTO users (name, username, password, email)
        VALUES (:name, :username, :password, :email)');

        $stmt->bindValue(':name', $body->name);
        $stmt->bindValue(':username', $body->username);
        $stmt->bindValue(':password', password_hash($body->password, PASSWORD_BCRYPT));
        $stmt->bindValue(':email', $body->email);

        return json_encode($stmt->executeQuery()->fetchAllAssociative());
    }

    public function put($id, $body)
    {
        $conn = new DB();
        $stmt = $conn->connection->prepare('UPDATE users SET name = :name, username = :username, password = :password, email = :email WHERE id = :id');

        $stmt->bindValue(':id', $id);
        $stmt->bindValue(':name', $body->name);
        $stmt->bindValue(':username', $body->username);
        $stmt->bindValue(':password', password_hash($body->password, PASSWORD_BCRYPT));
        $stmt->bindValue(':email', $body->email);

        return json_encode($stmt->executeQuery()->fetchAllAssociative());
    }

    public function delete($id)
    {
        $conn = new DB();
        $stmt = $conn->connection->prepare('DELETE FROM users WHERE id = :id');
        $stmt->bindValue(':id', $id);

        return json_encode($stmt->executeQuery()->fetchAllAssociative());
    }

    public function login($body)
    {
        $conn = new DB();
        $stmt = $conn->connection->prepare('SELECT id, username, password, token, token_expired_time FROM users WHERE username = :username');
        $stmt->bindValue(':username', $body->username);
        $resul = $stmt->executeQuery()->fetchAllAssociative();


        if (!$resul || !password_verify($body->password, $resul[0]['password'])) {

            http_response_code(400);
            return json_encode(
                array(
                    'http' => array('code' => '400', 'msg' => 'Bad Request'),
                    'error' => 'Incorrect <username> or <password>'
                )
            );
        }

        $token = JwtHelper::generateJWT($resul);

        $stmt = $conn->connection->prepare('UPDATE users SET token = :token, token_expired_time = :token_time WHERE id = :id');
        $stmt->bindValue(':id', $resul[0]['id']);
        $stmt->bindValue(':token', $token);

        $newDateTime = (new DateTime())->add(new DateInterval('PT120M'))->format('Y-m-d H:i:s');
        $stmt->bindValue(':token_time', $newDateTime);

        if ($stmt->executeQuery()->fetchAllAssociative()) {
            return json_encode(
                array(
                    'token' => $token,
                    'session' => $newDateTime
                )
            );
        }

    }

    public function isLogin($body)
    {
        $conn = new DB();
        $stmt = $conn->connection->prepare('SELECT token, token_expired_time FROM users WHERE token = :token');
        $stmt->bindValue(':token', $body->token);
        $resul = $stmt->executeQuery()->fetchAllAssociative();

        if (!$resul || new DateTime() < new DateTime($resul[0]['token_expired_time'])) {
            http_response_code(202);
            return json_encode(
                array(
                    'http' => array('code' => '202', 'msg' => 'Accepted'),
                    'auth' => 'OK'
                )
            );
        } else {
            http_response_code(403);
            return json_encode(
                array(
                    'http' => array('code' => '403', 'msg' => 'Forbidden'),
                    'auth' => 'Error'
                )
            );
        }
    }
}