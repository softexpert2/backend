<?php

namespace App\Softexpert\Controllers;

use App\Softexpert\Database\DB;
use App\Softexpert\Interfaces\iGenericRest;

class ProductController implements iGenericRest
{
    public function getAll()
    {
        $conn = new DB();
        $stmt = $conn->connection->prepare('SELECT p.*, t.name AS category_name FROM products p JOIN category t ON p.category_id = t.id');
        return json_encode($stmt->executeQuery()->fetchAllAssociative());
    }

    public function getOne($id)
    {
        $conn = new DB();
        $stmt = $conn->connection->prepare('SELECT p.*, t.name AS category_name FROM products p JOIN category t ON p.category_id = t.id WHERE p.id = :id');
        $stmt->bindValue(':id', $id);
        return json_encode($stmt->executeQuery()->fetchAllAssociative());
    }

    public function post($body)
    {
        $conn = new DB();
        $stmt = $conn->connection->prepare('INSERT INTO products (name, description, value, taxes, category_id )
        VALUES (:name, :description, :value, :taxes, :category_id)');

        $stmt->bindValue(':name', $body->name);
        $stmt->bindValue(':description', $body->description);
        $stmt->bindValue(':taxes', $body->taxes);
        $stmt->bindValue(':value', $body->value);
        $stmt->bindValue(':category_id', $body->category_id);

        return json_encode($stmt->executeQuery()->fetchAllAssociative());
    }

    public function put($id, $body)
    {
        $conn = new DB();
        $stmt = $conn->connection->prepare('UPDATE products SET name = :name, description = :description, value = :value, taxes = :taxes, category_id = :category_id WHERE id = :id');

        $stmt->bindValue(':id', $id);
        $stmt->bindValue(':name', $body->name);
        $stmt->bindValue(':description', $body->description);
        $stmt->bindValue(':value', $body->value);
        $stmt->bindValue(':taxes', $body->taxes);
        $stmt->bindValue(':category_id', $body->category_id);

        return json_encode($stmt->executeQuery()->fetchAllAssociative());
    }

    public function delete($id)
    {
        $conn = new DB();
        $stmt = $conn->connection->prepare('DELETE FROM products WHERE id = :id');
        $stmt->bindValue(':id', $id);

        return json_encode($stmt->executeQuery()->fetchAllAssociative());
    }

}