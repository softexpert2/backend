<?php
namespace App\Softexpert\Controllers;

use App\Softexpert\Database\DB;
use App\Softexpert\Interfaces\iGenericRest;
use ErrorException;

class CategoryController implements iGenericRest
{

    public function getAll()
    {
        $conn = new DB();
        $stmt = $conn->connection->prepare('SELECT * FROM category');
        return json_encode($stmt->executeQuery()->fetchAllAssociative());
    }

    public function getOne($id)
    {
        $conn = new DB();
        $stmt = $conn->connection->prepare('SELECT * FROM category WHERE id = :id');
        $stmt->bindValue(':id', $id);
        return json_encode($stmt->executeQuery()->fetchAllAssociative());
    }

    public function post($body)
    {
        $conn = new DB();
        $stmt = $conn->connection->prepare('INSERT INTO category (name)
        VALUES (:name)');

        $stmt->bindValue(':name', $body->name);

        return json_encode($stmt->executeQuery()->fetchAllAssociative());
    }

    public function put($id, $body)
    {
        throw new ErrorException("Not Implemented");
    }

    public function delete($id)
    {
        $conn = new DB();
        $stmt = $conn->connection->prepare('DELETE FROM category WHERE id = :id');
        $stmt->bindValue(':id', $id);

        return json_encode($stmt->executeQuery()->fetchAllAssociative());
    }

}